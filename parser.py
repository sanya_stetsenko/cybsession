import json
import os
from pprint import pprint
import re
import traceback

from bs4 import BeautifulSoup


class Iterator(object):
    def __init__(self, collection):
        self.collection = collection
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        try:
            result = self.collection[self.index]
            self.index += 1
        except IndexError:
            raise StopIteration
        return result

    def current(self):
        try:
            return self.collection[self.index]
        except IndexError:
            raise StopIteration

    def next(self):
        return self.__next__()

    def prev(self):
        self.index -= 1
        if self.index < 0:
            raise StopIteration
        return self.collection[self.index]


class Parser(object):
    def __init__(self, data):
        self.data = data
        self.iterator = Iterator(data)

    @property
    def headers(self):
        raise NotImplementedError('Headers is not defined')

    @property
    def event_types(self):
        raise NotImplementedError('Event types is not defined')

    def _clean_string(self, s):
        s = s.replace(u'\xa0', ' ')  # Replacing NO-BREAK SPACE
        return s.strip().replace('\n', ' ').replace('\t', '').replace('  ', '')

    def _format_short_name(self, name):
        if name is None:
            return None
        if len(name.split(' ')) == 1:
            return name
        return ''.join([item[0].upper() for item in name.split(' ') if len(item) > 2])

    def _format_teacher(self, fullname):
        if not fullname:
            return None
        fullname = fullname.strip()
        match = re.search(r'([\w]+)?\.?\s*([\w]+)\s*([\w])?\.?\s*([\w])\.?', fullname)
        if not match:
            return None
        degree, last, first, middle = match.groups()
        fullname = '{last} {first}{middle}'.format(last=last,
                                                    first=first + '.' if first else '',
                                                    middle=middle + '.' if middle else '')
        return {
            'full_name': fullname,
            'last_name': last,
            'fisrt_name': first,
            'middle_name': middle,
            'degree': TeacherType.type_by_name(degree),
        }

    def _find_headers(self):
        succ_found = 0
        for element in self.iterator:
            element = self._clean_string(element)
            if succ_found == len(self.headers):
                self.iterator.prev()
                return True
            if element == self.headers[succ_found]:
                succ_found += 1
            else:
                succ_found = 0
        return False

    def _skip_spaces(self):
        for element in self.iterator:
            if self._clean_string(element):
                self.iterator.prev()
                return True

    def _get_record(self):
        record = []
        for head in self.headers:
            if head in self.event_types:
                continue
            line = self._clean_string(self.iterator.current())
            if 'END_OF_LIST' in line:
                return record
            record.append(line or None)
            self.iterator.next()
        return record

    def _get_event_property(self, data, event_name, property):
        counter = self.event_types.index(event_name)
        lst = list(self.headers)
        for event in self.event_types:
            lst.remove(event)

        delta = 0
        t = tuple(lst)
        for i in range(counter):
            delta += t.index(property) + 1
            t = t[t.index(property) + 1:]

        index = t.index(property)
        return data[delta + index]

    def processing(self):
        if not self._find_headers():
            print('Headers records was not found')
            return

        self._skip_spaces()
        try:
            course = None
            group = None
            while True:
                record = self._get_record()
                if len(record) != len(self.headers) - len(self.event_types):
                    return
                course = record[self.headers.index('Курс')] or course
                group = record[self.headers.index('Група')] or group
                for event in self.event_types:
                    event_struct = dict(
                        course=course,
                        group=group,
                        name=record[self.headers.index('Дисципліна')],
                        short_name=self._format_short_name(record[self.headers.index('Дисципліна')]),
                        date=self._get_event_property(record, event, 'Дата'),
                        time=self._get_event_property(record, event, 'Год.'),
                        room_data=[dict(housing=None, room_floor=None,
                                        room_num=self._get_event_property(record, event, 'Ауд.'),
                                        )],
                        format=EventType.type_by_name(event),
                        teachers=[
                            self._format_teacher(fullname)
                            for fullname in record[self.headers.index('Викладач')].split(',')
                            if bool(fullname.strip())
                        ]
                    )
                    if len(event_struct.get('teachers', [])) == 0:
                        return
                    yield event_struct
        except StopIteration:
            pass


class ExamsParser(Parser):
    def __init__(self, data):
        super().__init__(data)

    @property
    def headers(self):
        return ('Курс', 'Група', 'Дисципліна', 'Викладач',
                'Консультації', 'Екзамени',
                'Дата', 'Год.', 'Ауд.', 'Дата', 'Год.', 'Ауд.')

    @property
    def event_types(self):
        return ('Консультації', 'Екзамени',)


class CreditsParser(Parser):
    def __init__(self, data):
        super().__init__(data)

    @property
    def headers(self):
        return ('Курс', 'Група', 'Дисципліна', 'Викладач',
                'Заліки',
                'Дата', 'Год.', 'Ауд.')

    @property
    def event_types(self):
        return ('Заліки',)


class BaseType(object):
    unknown = -1, 'unknown'

    def __getattribute__(cls, name):
        value = object.__getattribute__(cls, name) or cls.unknown
        return value[0]

    @classmethod
    def type_by_name(cls, name):
        attr_dict = cls.to_dict()
        return attr_dict.get(name, None)

    @classmethod
    def to_dict(cls):
        return {i[1]: i[0] for desc, i in cls.__dict__.items()
                if desc not in ("type_by_name", "to_dict") and not desc.startswith("__")}


class EventType(BaseType):
    unknown = -1, 'Інше'
    consultation = 0, 'Консультації'
    credit = 1, 'Заліки'
    exam = 2, 'Екзамени'


class TeacherType(BaseType):
    teacher = 0, 'вик'
    # main_teacher = 1, 'ст-вик'
    docent = 2, 'доц'
    professor = 3, 'проф'
    assistant = 4, 'ас'
    unknown = 5, 'інше'


class FileReader(object):
    def __init__(self, filepath):
        self.filepath = filepath
        self.data = None

    def _get_html_parser(self):
        with open(self.filepath, 'r', encoding='windows-1251') as file:
            html_text = file.read()
        return BeautifulSoup(html_text, 'html.parser')

    def select_data(self):
        soup = self._get_html_parser()
        span_lst = soup.findAll('span')
        filtered_lst = []
        for i, elem in enumerate(span_lst):
            if 'br clear="all"' in str(elem):
                # masked end of table
                filtered_lst.append('END_OF_LIST')
            elif str(elem) == '<span lang="EN-US" style=\'font-size:10.0pt;font-family:"Times New Roman",serif\'> </span>':
                # ignored BAD-space
                continue
            else:
                filtered_lst.append(elem.text)
        self.data = filtered_lst
        return filtered_lst


if __name__ == '__main__':
    html_folder = os.path.join(os.getcwd(), 'html')
    json_folder = os.path.join(os.getcwd(), 'json')
    general_file = os.path.join(json_folder, 'All.json')

    if not os.path.exists(json_folder):
        os.mkdir(json_folder)

    files = [os.path.join(html_folder, f)
                 for f in os.listdir(html_folder)
                 if os.path.isfile(os.path.join(html_folder, f))]

    general_file = open(general_file, 'w')
    all_events = []
    for file in files:
        print('Start processing: {}'.format(file))
        try:
            output_filename = ('{}.json'.format(os.path.join(json_folder, os.path.basename(os.path.splitext(file)[0]))))
            output = open(output_filename, 'w')
            data = FileReader(file).select_data()

            exams = [event for event in ExamsParser(data).processing()]
            credits = [event for event in CreditsParser(data).processing()]

            print(json.dumps(exams+credits, indent=2, sort_keys=True, ensure_ascii=False,), file=output)

            all_events.append(exams)
            all_events.append(credits)

            # # Use for step-by-step printing
            # for event in ExamsParser(data).processing():
            #     print(json.dumps(event, ensure_ascii=False, indent=4, sort_keys=True), file=output)
            #     output.flush()
            #     print(json.dumps(event, ensure_ascii=False, indent=4, sort_keys=True), file=general_file)
            #     output.flush()
            #
            # for event in CreditsParser(data).processing():
            #     print(json.dumps(event, ensure_ascii=False, indent=4, sort_keys=True), file=output)
            #     output.flush()
            #     print(json.dumps(event, ensure_ascii=False, indent=4, sort_keys=True), file=general_file)
            #     output.flush()

            output.close()
        except Exception as ex:
            print('Something wrong: {} \n {}'.format(str(ex), traceback.format_exc()))

    print(json.dumps(all_events, indent=2, sort_keys=True, ensure_ascii=False, ), file=general_file)
    general_file.close()
